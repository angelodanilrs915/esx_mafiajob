Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerSizeL                 = { x = 3.0, y = 3.0, z = 1.0 }
Config.MarkerColor                = { r = 204, g = 50, b = 50 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.Dealers = {
	DrugDealer_Mafia = {coords = vector3(-35.3, -2552.26, 5.06), name = _U('blip_drugdealer'), color = 6, sprite = 355, radius = 25.0},
}

Config.MafiaStations = {
  Mafia = {
    Blip = {
      Pos     = { x = -2679.44, y = 1337.25, z = 151.01 },
      Sprite  = 78,
      Display = 4,
      Scale   = 1.2,
      Colour  = 1
    },

    Armories = {
      { x = -2675.82, y = 1331.03, z = 139.88 }
    },

    BossActions = {
      { x = -2679.44, y = 1337.25, z = 151.01 }
    },

		Vehicles = {
			{
				Spawner    = { x = -2665.87, y = 1301.69, z = 146.45 },
				SpawnPoints = {
					{ x = -2660.6, y = 1307.26, z = 146.12, heading = 270.35, radius = 6.0 }
				}
			}
		},

		VehicleDeleters = {
			{ x = -2670.69, y = 1309.69, z = 146.12 },
			{ x = -2670.69, y = 1305.69, z = 146.12 }
		}
  }
}
